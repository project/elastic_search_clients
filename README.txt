README
------

Installation
------------

Step 1) Download composer_manager module 
http://drupal.org/project/composer_manager

Step 2) Download elastic_search_clients module

Step 3) Use drush command in following order
          drush en elastic_search_clients -y
          drush composer-json-rebuild
          drush composer-manager update

You can later keep specific libraries using
admin settings from elastic_search_clients.
